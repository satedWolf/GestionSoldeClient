﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDesSoldeClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnQuitter_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnCreerPatient_Click(object sender, EventArgs e)
        {
            CreerClient form = new CreerClient();
            form.Show();
        }

        private void btnCategories_Click(object sender, EventArgs e)
        {
            CreerCategorie form = new CreerCategorie();
            form.Show();
        }

        private void btnAPayer_Click(object sender, EventArgs e)
        {
            APayerForm form = new APayerForm();
            form.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RapportForm form = new RapportForm();
            form.Show();
        }

        private void btnPayer_Click(object sender, EventArgs e)
        {
           PayerForm form = new PayerForm();
           form.Show();
        }
    }
}
