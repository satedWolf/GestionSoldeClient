﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDesSoldeClient
{
    class Rapport
    {

        public void soldeNonClaire(DateTime date)
        {
            var db = new DB();
            var clients = db.GetClients();
            Document document = new Document(PageSize.LETTER, 10f, 10f, 35f, 35f);
            PdfWriter.GetInstance(document, (Stream)new FileStream("Rapport\\SoldeNonClaire-" + date.ToString("yyyy-MM-dd") + ".pdf", FileMode.Create));
            Paragraph Client = new Paragraph( "Client dont le solde n'est pas a zéro le "+date+ "\n\n");
            document.Open();
            foreach (var client in clients)
            {
                var solde = client.GetSolde(date.Month, date.Year);
                if (client.GetSolde(date.Month, date.Year) != 0)
                {
                    Client.Add(client.Id + " - " + client.Prenom + " " + client.Nom + " - Solde : " + client.GetSolde(date.Month, date.Year) + "$\n");
                }
            }
            document.Add(Client);
            document.Close();
            Process.Start("Rapport\\SoldeNonClaire-" + date.ToString("yyyy-MM-dd") + ".pdf");
        }

        public void profilClient(DateTime date, int id)
        {
            var db = new DB();
            var client = db.GetClients().Where(x => x.Id == id).Single();
            var transacs = db.GetTransaction().Where(x => x.Client_Id == id).ToList();
            Document document = new Document(PageSize.LETTER, 10f, 10f, 35f, 35f);
            PdfWriter.GetInstance(document, (Stream)new FileStream("Rapport\\ProfilClient\\" + client.GetNom() + ".pdf", FileMode.Create));
            Paragraph transacParagraphe = new Paragraph(client.GetNom() + "\n\n");
            document.Open();
            foreach (var transac in transacs)
            {
              if(transac.Montant > 0)
                {
                    transacParagraphe.Add(transac.Date + " Payer : (" + transac.Montant + "$) Type de paiement : " + transac.TypePayment + " Initiale :" + transac.Initiale + "\n");
                }else
                {
                    transacParagraphe.Add(transac.Date + " Dette : (" + transac.Montant + "$) \n");
                }
                   
                
            }
            document.Add(transacParagraphe);
            document.Close();
            Process.Start("Rapport\\ProfilClient\\" + client.GetNom() + ".pdf");
        }
    }
}
