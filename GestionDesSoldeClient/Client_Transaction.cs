﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDesSoldeClient
{
    class Client_Transaction
    {
        public int Id { get; set; }
        public int Client_Id { get; set; }
        public Decimal Montant { get; set; }
        public String Initiale { get; set; }
        public String TypePayment { get; set; }
        public DateTime Date { get; set; }
    }
}
