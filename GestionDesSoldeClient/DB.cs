﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDesSoldeClient
{
    class DB
    {
        //Getters de la base de donnees
        public List<Categorie> GetCategories()
        {
            var list = new List<Categorie>();
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("SELECT * FROM Categorie"))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (OleDbDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            var tempCat = new Categorie();
                            tempCat.id = Int32.Parse(sdr["ID"].ToString());
                            tempCat.Nom = sdr["Nom"].ToString();
                            list.Add(tempCat);
                        }
                    }
                    con.Close();
                }
            }
            return list;
        }
        public List<Client_Transaction> GetTransaction(){
            var list = new List<Client_Transaction>();
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("SELECT * FROM Client_Transaction"))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (OleDbDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            var tempTransac = new Client_Transaction();
                            tempTransac.Id = Int32.Parse(sdr["ID"].ToString());
                            tempTransac.Montant = Decimal.Parse(sdr["Montant"].ToString());
                            tempTransac.Client_Id = Int32.Parse(sdr["Client_Id"].ToString());
                            tempTransac.TypePayment = sdr["TypePayment"].ToString();
                            tempTransac.Initiale = sdr["Initiales"].ToString();
                            tempTransac.Date = DateTime.Parse(sdr["Date"].ToString());
                            list.Add(tempTransac);
                        }
                    }
                    con.Close();
                }
            }
            return list;

             
        }
        public List<Client> GetClients()
        {
            var list = new List<Client>();
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("SELECT * FROM Clients"))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (OleDbDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            var tempClient = new Client();
                            tempClient.Id = Int32.Parse(sdr["ID"].ToString());
                            tempClient.Nom = sdr["Nom"].ToString();
                            tempClient.Prenom = sdr["Prenom"].ToString();
                            tempClient.Categorie_Id = Int32.Parse(sdr["Categorie_Id"].ToString());
                            list.Add(tempClient);
                        }
                    }
                    con.Close();
                }
            }
            return list;
        }

        //Ajouts dans la base de donnees
        public void AjouterClient(Client client)
        {
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("insert into Clients (Nom,Prenom,Categorie_Id) values ('" + client.Nom+ "', '"+client.Prenom+"', '"+client.Categorie_Id+"')"))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
    }
        public void AjouterCategorie(Categorie cat)
        {
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("insert into Categorie (Nom) values ('" + cat.Nom + "')"))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }
        public void AjouterTransaction(Client_Transaction transaction)
        {
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("insert into Client_Transaction (Client_Id, Montant, Initiales, `Date`,TypePayment ) values (" + transaction.Client_Id + ", "+transaction.Montant+", '"+transaction.Initiale+"', '"+transaction.Date+"', '"+transaction.TypePayment+"')"))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }

    }
}
