﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDesSoldeClient
{
    public partial class CreerCategorie : Form
    {
        public CreerCategorie()
        {
            InitializeComponent();
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            var db = new DB();
            var cat = new Categorie();
            cat.Nom = textNom.Text;
            db.AjouterCategorie(cat);
            textNom.Text = "";
        }
    }
}
