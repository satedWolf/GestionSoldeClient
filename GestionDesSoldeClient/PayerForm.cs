﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDesSoldeClient
{
    public partial class PayerForm : Form
    {
        public PayerForm()
        {
            InitializeComponent();
        }

        private void PayerForm_Load(object sender, EventArgs e)
        {
            var db = new DB();
            cmbCategories.Items.AddRange(db.GetCategories().Select(x => x.id + "-" + x.Nom).ToArray());
            TypePayement.Items.Add("CARTE");
            TypePayement.Items.Add("CASH");
            TypePayement.Items.Add("CHEQUE");
            cmbClient.Enabled = false;
            txtMontant.Enabled = false;
            Initiale.Enabled = false;
            TypePayement.Enabled = false;
        }

        private void cmbCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            var db = new DB();
            cmbClient.Items.Clear();
            txtMontant.Text = "";
            TypePayement.SelectedIndex = -1;
            cmbClient.Items.AddRange(db.GetClients().Where(x => x.Categorie_Id == Int32.Parse(cmbCategories.SelectedItem.ToString().Split('-')[0])).Select(x => x.Id + "-" + x.Prenom + " " + x.Nom).ToArray());
            cmbClient.Enabled = true;
            txtMontant.Enabled = true;
            Initiale.Enabled = true;
            TypePayement.Enabled = true;
        }

        private void Enregistrer_Click(object sender, EventArgs e)
        {
            try {
                var db = new DB();
                var transaction = new Client_Transaction();
                transaction.Client_Id = Int32.Parse(cmbClient.SelectedItem.ToString().Split('-')[0]);
                transaction.Initiale = Initiale.Text;
                transaction.TypePayment = TypePayement.SelectedItem.ToString();
                transaction.Montant = Decimal.Parse(txtMontant.Text);
                transaction.Date = DateTime.Now;
                db.AjouterTransaction(transaction);
                txtMontant.Text = "";
                TypePayement.SelectedIndex = -1;
            } catch (Exception ) {
               MessageBox.Show("Le montant entré est incorrecte merci de le corriger !");
           }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
