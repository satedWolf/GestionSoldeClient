﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDesSoldeClient
{
    public partial class APayerForm : Form
    {
        public APayerForm()
        {
            InitializeComponent();
        }

        private void APayerForm_Load(object sender, EventArgs e)
        {
            var db = new DB();
            cmbCategories.Items.AddRange(db.GetCategories().Select(x => x.id + "-" + x.Nom).ToArray());
            Date.Format = DateTimePickerFormat.Custom;
            Date.CustomFormat = "MM/yyyy";

        }

        private void btnRecherche_Click(object sender, EventArgs e)
        {
            var db = new DB();
            var clients = db.GetClients().Where(x => x.Categorie_Id == Int32.Parse(cmbCategories.SelectedItem.ToString().Split('-')[0])).ToList();
            tabValeur.Rows.Clear();
            foreach(var client in clients)
            {
                tabValeur.Rows.Add(client.Id, client.Nom, client.Prenom, "");
            }

        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            var db = new DB();
            for(int i = 0; i< tabValeur.Rows.Count-1; i++)
            {
                var row = tabValeur.Rows[i];
                if (!String.IsNullOrEmpty(row.Cells[3].Value.ToString()))
                {
                    var transaction = new Client_Transaction();
                    
                    transaction.Client_Id = Int32.Parse(row.Cells[0].Value.ToString());
                    transaction.Montant = Decimal.Parse(row.Cells[3].Value.ToString()) * -1;
                    transaction.TypePayment = "Montant a payer";
                    transaction.Date = Date.Value;
                    transaction.Initiale = "";
                    
                    db.AjouterTransaction(transaction);
                   
                }
            }
            this.Close();

        }

        private void btnRetour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
