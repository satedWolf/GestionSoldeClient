﻿namespace GestionDesSoldeClient
{
    partial class RapportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateProfil = new System.Windows.Forms.DateTimePicker();
            this.dateSoldeNonClairé = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.btnNonClairer = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbCategories = new System.Windows.Forms.ComboBox();
            this.cmbClients = new System.Windows.Forms.ComboBox();
            this.btnProfilClient = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dateProfil
            // 
            this.dateProfil.Location = new System.Drawing.Point(120, 251);
            this.dateProfil.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateProfil.Name = "dateProfil";
            this.dateProfil.Size = new System.Drawing.Size(265, 22);
            this.dateProfil.TabIndex = 0;
            // 
            // dateSoldeNonClairé
            // 
            this.dateSoldeNonClairé.Location = new System.Drawing.Point(32, 126);
            this.dateSoldeNonClairé.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateSoldeNonClairé.Name = "dateSoldeNonClairé";
            this.dateSoldeNonClairé.Size = new System.Drawing.Size(265, 22);
            this.dateSoldeNonClairé.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.btnNonClairer);
            this.panel1.Controls.Add(this.dateSoldeNonClairé);
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(536, 30);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(343, 367);
            this.panel1.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 105);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Date";
            // 
            // btnNonClairer
            // 
            this.btnNonClairer.Location = new System.Drawing.Point(95, 177);
            this.btnNonClairer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnNonClairer.Name = "btnNonClairer";
            this.btnNonClairer.Size = new System.Drawing.Size(147, 43);
            this.btnNonClairer.TabIndex = 0;
            this.btnNonClairer.Text = "Solde non clairé";
            this.btnNonClairer.UseVisualStyleBackColor = true;
            this.btnNonClairer.Click += new System.EventHandler(this.btnNonClairer_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.cmbCategories);
            this.panel2.Controls.Add(this.cmbClients);
            this.panel2.Controls.Add(this.btnProfilClient);
            this.panel2.Controls.Add(this.dateProfil);
            this.panel2.Location = new System.Drawing.Point(32, 30);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(495, 367);
            this.panel2.TabIndex = 3;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(116, 232);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(116, 168);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Clients";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(116, 102);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Catégories";
            // 
            // cmbCategories
            // 
            this.cmbCategories.FormattingEnabled = true;
            this.cmbCategories.Location = new System.Drawing.Point(120, 122);
            this.cmbCategories.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbCategories.Name = "cmbCategories";
            this.cmbCategories.Size = new System.Drawing.Size(265, 24);
            this.cmbCategories.TabIndex = 4;
            this.cmbCategories.SelectedIndexChanged += new System.EventHandler(this.cmbCategories_SelectedIndexChanged);
            // 
            // cmbClients
            // 
            this.cmbClients.FormattingEnabled = true;
            this.cmbClients.Location = new System.Drawing.Point(120, 187);
            this.cmbClients.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbClients.Name = "cmbClients";
            this.cmbClients.Size = new System.Drawing.Size(265, 24);
            this.cmbClients.TabIndex = 3;
            // 
            // btnProfilClient
            // 
            this.btnProfilClient.Location = new System.Drawing.Point(177, 307);
            this.btnProfilClient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnProfilClient.Name = "btnProfilClient";
            this.btnProfilClient.Size = new System.Drawing.Size(147, 43);
            this.btnProfilClient.TabIndex = 2;
            this.btnProfilClient.Text = "Imprimer";
            this.btnProfilClient.UseVisualStyleBackColor = true;
            this.btnProfilClient.Click += new System.EventHandler(this.btnProfilClient_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(65, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(376, 48);
            this.label5.TabIndex = 8;
            this.label5.Text = "Historique du client";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(341, 42);
            this.label6.TabIndex = 7;
            this.label6.Text = "Rapport des soldes";
            // 
            // RapportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(896, 410);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "RapportForm";
            this.Text = "RapportForm";
            this.Load += new System.EventHandler(this.RapportForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateProfil;
        private System.Windows.Forms.DateTimePicker dateSoldeNonClairé;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnNonClairer;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnProfilClient;
        private System.Windows.Forms.ComboBox cmbClients;
        private System.Windows.Forms.ComboBox cmbCategories;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
    }
}