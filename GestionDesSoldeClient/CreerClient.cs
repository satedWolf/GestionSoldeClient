﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDesSoldeClient
{
    public partial class CreerClient : Form
    {
        public CreerClient()
        {
            InitializeComponent();
        }

        private void CreerClient_Load(object sender, EventArgs e)
        {
            var db = new DB();
            cmbCategories.Items.AddRange(db.GetCategories().Select(x => x.id + "-" + x.Nom).ToArray());
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            var db = new DB();
            var client = new Client();
            client.Nom = txtNom.Text;
            client.Prenom = txtPrenom.Text;
            client.Categorie_Id = Int32.Parse(cmbCategories.SelectedItem.ToString().Split('-')[0]);
            db.AjouterClient(client);
            txtNom.Text = "";
            txtPrenom.Text = "";
            cmbCategories.SelectedIndex = -1;
        }
    }
}
