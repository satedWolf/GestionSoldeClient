﻿namespace GestionDesSoldeClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnCreerPatient = new System.Windows.Forms.Button();
            this.btnAPayer = new System.Windows.Forms.Button();
            this.btnQuitter = new System.Windows.Forms.Button();
            this.btnPayer = new System.Windows.Forms.Button();
            this.btnCategories = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BtnCreerPatient
            // 
            this.BtnCreerPatient.Location = new System.Drawing.Point(14, 87);
            this.BtnCreerPatient.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.BtnCreerPatient.Name = "BtnCreerPatient";
            this.BtnCreerPatient.Size = new System.Drawing.Size(294, 77);
            this.BtnCreerPatient.TabIndex = 0;
            this.BtnCreerPatient.Text = "Créer Client";
            this.BtnCreerPatient.UseVisualStyleBackColor = true;
            this.BtnCreerPatient.Click += new System.EventHandler(this.BtnCreerPatient_Click);
            // 
            // btnAPayer
            // 
            this.btnAPayer.Location = new System.Drawing.Point(14, 170);
            this.btnAPayer.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnAPayer.Name = "btnAPayer";
            this.btnAPayer.Size = new System.Drawing.Size(294, 77);
            this.btnAPayer.TabIndex = 1;
            this.btnAPayer.Text = "A Payer";
            this.btnAPayer.UseVisualStyleBackColor = true;
            this.btnAPayer.Click += new System.EventHandler(this.btnAPayer_Click);
            // 
            // btnQuitter
            // 
            this.btnQuitter.Location = new System.Drawing.Point(10, 502);
            this.btnQuitter.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnQuitter.Name = "btnQuitter";
            this.btnQuitter.Size = new System.Drawing.Size(294, 77);
            this.btnQuitter.TabIndex = 3;
            this.btnQuitter.Text = "Quitter";
            this.btnQuitter.UseVisualStyleBackColor = true;
            this.btnQuitter.Click += new System.EventHandler(this.btnQuitter_Click);
            // 
            // btnPayer
            // 
            this.btnPayer.Location = new System.Drawing.Point(14, 253);
            this.btnPayer.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnPayer.Name = "btnPayer";
            this.btnPayer.Size = new System.Drawing.Size(294, 77);
            this.btnPayer.TabIndex = 4;
            this.btnPayer.Text = "Payer";
            this.btnPayer.UseVisualStyleBackColor = true;
            this.btnPayer.Click += new System.EventHandler(this.btnPayer_Click);
            // 
            // btnCategories
            // 
            this.btnCategories.Location = new System.Drawing.Point(12, 336);
            this.btnCategories.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnCategories.Name = "btnCategories";
            this.btnCategories.Size = new System.Drawing.Size(294, 77);
            this.btnCategories.TabIndex = 5;
            this.btnCategories.Text = "Ajouter une categorie";
            this.btnCategories.UseVisualStyleBackColor = true;
            this.btnCategories.Click += new System.EventHandler(this.btnCategories_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(10, 419);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(294, 77);
            this.button1.TabIndex = 6;
            this.button1.Text = "Imprimer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(299, 51);
            this.label1.TabIndex = 7;
            this.label1.Text = "Soldes Clients";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 586);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnCategories);
            this.Controls.Add(this.btnPayer);
            this.Controls.Add(this.btnQuitter);
            this.Controls.Add(this.btnAPayer);
            this.Controls.Add(this.BtnCreerPatient);
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnCreerPatient;
        private System.Windows.Forms.Button btnAPayer;
        private System.Windows.Forms.Button btnQuitter;
        private System.Windows.Forms.Button btnPayer;
        private System.Windows.Forms.Button btnCategories;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
    }
}

