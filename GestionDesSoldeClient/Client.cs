﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDesSoldeClient
{
    class Client
    {
        public int Id { get; set; }
        public String Nom { get; set; }
        public String Prenom { get; set; }
        public int Categorie_Id { get; set; }


        public decimal GetSolde(int month , int year )
        {
            
            var DB = new DB();
            var transac = DB.GetTransaction().Where(x => x.Client_Id == this.Id && x.Date.Month >= month && x.Date.Year >= year).ToList();
            var solde = transac.Sum(x => x.Montant);
            return solde;
        }


        public String GetNom()
        {
            return this.Prenom + " " + this.Nom;
        }
    }

   
}
