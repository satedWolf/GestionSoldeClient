﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDesSoldeClient
{
    public partial class RapportForm : Form
    {
        public RapportForm()
        {
            InitializeComponent();
        }

        private void btnNonClairer_Click(object sender, EventArgs e)
        {
            var doc = new Rapport();
            doc.soldeNonClaire(dateSoldeNonClairé.Value);
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void RapportForm_Load(object sender, EventArgs e)
        {
            var db = new DB();
            cmbCategories.Items.AddRange(db.GetCategories().Select(x => x.id + "-" + x.Nom).ToArray());
            cmbClients.Enabled = false;
        }

        private void cmbCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            var db = new DB();
            cmbClients.Items.Clear();
            cmbClients.Enabled = true;
            cmbClients.Items.AddRange(db.GetClients().Where(x => x.Categorie_Id == Int32.Parse(cmbCategories.SelectedItem.ToString().Split('-')[0])).Select(x => x.Id + "-" + x.Prenom + " " + x.Nom).ToArray());
        }

        private void btnProfilClient_Click(object sender, EventArgs e)
        {
            var rapport = new Rapport();
            rapport.profilClient(dateProfil.Value, Int32.Parse(cmbClients.SelectedItem.ToString().Split('-')[0]));
        }
    }
}
